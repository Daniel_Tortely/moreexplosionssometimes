package explosions;

/**
 *
 * @author Roland Fuller
 */
public class Main {

    public static void main(String[] args) {
        ExplosionsSometimes app = new ExplosionsSometimes();
        app.start();
    }

}
