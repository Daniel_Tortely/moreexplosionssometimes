package explosions;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

/**
 *
 * @author Roland Fuller
 */
@Serializable
public abstract class NetworkMessage extends AbstractMessage {

    public NetworkMessage() {
    }

}
