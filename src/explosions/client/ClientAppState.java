package explosions.client;

import explosions.client.entity.UpdateControl;
import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.audio.AudioNode;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.math.Vector3f;
import com.jme3.network.Client;
import com.jme3.network.ClientStateListener;
import com.jme3.network.ErrorListener;
import com.jme3.network.Message;
import com.jme3.network.MessageListener;
import com.jme3.network.Network;
import com.jme3.scene.Node;
import com.jme3.scene.SceneGraphVisitor;
import com.jme3.scene.Spatial;
import static explosions.Factory.ID;
import explosions.NetworkMessage;
import static explosions.client.entity.PlayerControl.BACKWARD;
import static explosions.client.entity.PlayerControl.FORWARD;
import static explosions.client.entity.PlayerControl.JUMP;
import static explosions.client.entity.PlayerControl.LEFT;
import static explosions.client.entity.PlayerControl.RELOAD;
import static explosions.client.entity.PlayerControl.RIGHT;
import static explosions.client.entity.PlayerControl.SHOOT;
import static explosions.client.entity.PlayerControl.SWITCH;
import explosions.client.message.ClientMessage.ChatMessage;
import explosions.client.message.ClientMessage.PlayerInputMessage;
import explosions.client.message.ClientMessage.PlayerLookMessage;
import static explosions.server.ServerAppState.GameObject.AMMO;
import static explosions.server.ServerAppState.GameObject.FLOOR;
import static explosions.server.ServerAppState.GameObject.MISSILE;
import static explosions.server.ServerAppState.GameObject.PLAYER;
import explosions.server.message.ServerMessage.AnimationMessage;
import explosions.server.message.ServerMessage.AudioMessage;
import explosions.server.message.ServerMessage.RemoveMessage;
import explosions.server.message.ServerMessage.SpawnMessage;
import explosions.server.message.ServerMessage.UpdateLocationMessage;
import java.io.IOException;
import java.util.concurrent.Callable;
import explosions.Broadcaster;

/**
 *
 * @author Roland Fuller
 */
public class ClientAppState extends AbstractAppState implements Broadcaster, MessageListener<Client>, ErrorListener<Object>, ClientStateListener {

    private static final int CAM_SPEED = 0;
    private ClientFactory clientFactory;
    private Node clientRootNode;
    private Client myClient;
    private SimpleApplication app;

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);

        this.app = (SimpleApplication) app;
        clientRootNode = new Node();
        ((SimpleApplication) app).getRootNode().attachChild(clientRootNode);

        clientFactory = new ClientFactory(app.getCamera(), app.getAssetManager(), app.getInputManager(), this, clientRootNode);

        app.getInputManager().addMapping(JUMP, new KeyTrigger(KeyInput.KEY_SPACE));
        app.getInputManager().addMapping(FORWARD, new KeyTrigger(KeyInput.KEY_W));
        app.getInputManager().addMapping(BACKWARD, new KeyTrigger(KeyInput.KEY_S));
        app.getInputManager().addMapping(LEFT, new KeyTrigger(KeyInput.KEY_A));
        app.getInputManager().addMapping(RIGHT, new KeyTrigger(KeyInput.KEY_D));
        app.getInputManager().addMapping(SHOOT, new KeyTrigger(KeyInput.KEY_LSHIFT));
        app.getInputManager().addMapping(RELOAD, new KeyTrigger(KeyInput.KEY_E));
        app.getInputManager().addMapping(SWITCH, new KeyTrigger(KeyInput.KEY_Q));

        ((SimpleApplication) app).getFlyByCamera().setMoveSpeed(0);

        try {
            myClient = Network.connectToServer("localhost", 6143);
            myClient.addMessageListener(this, NetworkMessage.class);
            myClient.addMessageListener(this, SpawnMessage.class);
            myClient.addMessageListener(this, ChatMessage.class);
            myClient.addMessageListener(this, PlayerInputMessage.class);
            myClient.addMessageListener(this, PlayerLookMessage.class);
            myClient.addMessageListener(this, UpdateLocationMessage.class);
            myClient.addMessageListener(this, AudioMessage.class);
            myClient.addMessageListener(this, AnimationMessage.class);
            myClient.addMessageListener(this, RemoveMessage.class);
            myClient.addClientStateListener(this);
            myClient.addErrorListener(this);
            myClient.start();
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    private void removeGameObject(final int id) {
        clientRootNode.depthFirstTraversal(new SceneGraphVisitor() {
            @Override
            public void visit(Spatial spatial) {
                // TODO: better to build an entity registry rather than examine every component
                final Object spatialID = spatial.getUserData(ID);
                if (spatialID != null) {
                    if (id == (int) spatialID) {
                        UpdateControl ncc = spatial.getControl(UpdateControl.class);
                        ncc.timesUp();
                    }
                }
            }
        });
    }

    private void updateGameObjectAnimation(final int id, final String animation) {
        clientRootNode.depthFirstTraversal(new SceneGraphVisitor() {
            @Override
            public void visit(Spatial spatial) {
                // TODO: better to build an entity registry rather than examine every component
                final Object spatialID = spatial.getUserData(ID);
                if (spatialID != null) {
                    if (id == (int) spatialID) {
                        UpdateControl ncc = spatial.getControl(UpdateControl.class);
                        ncc.playAnimation(animation);
                    }
                }
            }
        });
    }

    private void updateGameObjectLocation(final int id, final Vector3f location) {

        clientRootNode.depthFirstTraversal(new SceneGraphVisitor() {
            @Override
            public void visit(Spatial spatial) {
                // TODO: better to build an entity registry rather than examine every component
                final Object spatialID = spatial.getUserData(ID);
                if (spatialID != null) {
                    if (id == (int) spatialID) {
                        UpdateControl ncc = spatial.getControl(UpdateControl.class);
                        ncc.move(location);
                    }
                }
            }
        });

        // gets given update messages through the network from the server side
        // then finds the appropriate game object and informs it of the change
    }

    @Override
    public void cleanup() {
        super.cleanup();
        if (myClient != null) {
            myClient.close();
        }
    }

    @Override
    public void sendUpdate(NetworkMessage networkMessage) {
        //networkConnection.recieveUpdate(networkMessage);
        myClient.send(networkMessage);
    }

    @Override
    public void messageReceived(Client source, final Message message) {
        app.enqueue(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                if (message instanceof ChatMessage) {
                    // do something with the message
                    ChatMessage helloMessage = (ChatMessage) message;
                    // System.out.println("Client #" + source.getId() + " received: '" + helloMessage.message + "'");
                } else if (message instanceof UpdateLocationMessage) {
                    UpdateLocationMessage ulm = (UpdateLocationMessage) message;
                    updateGameObjectLocation(ulm.id, ulm.location);
                } else if (message instanceof SpawnMessage) {
                    SpawnMessage sm = (SpawnMessage) message;
                    switch (sm.gameObject) {
                        case FLOOR:
                            clientFactory.makeFloor(sm.id);
                            break;
                        case PLAYER:
                            clientFactory.makePlayer(sm.id, sm.location, sm.isPlayer);
                            break;
                        case MISSILE:
                            clientFactory.makeRocket(sm.id, sm.location);
                            break;
                        case AMMO:
                            clientFactory.makeAmmo(sm.id, sm.location);
                            break;
                    }
                } else if (message instanceof AudioMessage) {
                    AudioMessage am = (AudioMessage) message;
                    AudioNode an = (AudioNode) clientRootNode.getChild(am.effect);
                    an.playInstance();
                } else if (message instanceof AnimationMessage) {
                    AnimationMessage am = (AnimationMessage) message;
                    updateGameObjectAnimation(am.id, am.animation);
                } else if (message instanceof RemoveMessage) {
                    RemoveMessage rm = (RemoveMessage) message;
                    removeGameObject(rm.id);
                }
                return null;
            }
        });
    }

    @Override
    public void handleError(Object source, Throwable t) {
    }

    @Override
    public void clientConnected(Client c) {
    }

    @Override
    public void clientDisconnected(Client c, DisconnectInfo info) {
    }
}
