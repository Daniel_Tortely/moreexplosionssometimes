package explosions;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.TextField;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

/**
 *
 * @author Roland Fuller
 */
public class MenuScreen implements ScreenController {

    private ExplosionsSometimes explosionsSometimes;
    private TextField serverPort;

    public MenuScreen(ExplosionsSometimes explosionsSometimes) {
        this.explosionsSometimes = explosionsSometimes;
    }

    public void onStartClient() {
        String ipAddress = "";
        explosionsSometimes.startClient(ipAddress);
    }

    public void onStartServer() {
        explosionsSometimes.startServer(serverPort.getRealText());
    }

    public void onQuit() {
        explosionsSometimes.quit();
    }

    @Override
    public void bind(Nifty nifty, Screen screen) {
        serverPort = screen.findNiftyControl("serverPort", TextField.class);
    }

    @Override
    public void onStartScreen() {
    }

    @Override
    public void onEndScreen() {
    }
}
