package explosions;

import com.jme3.app.SimpleApplication;
import com.jme3.network.serializing.Serializer;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.renderer.RenderManager;
import de.lessvoid.nifty.Nifty;
import explosions.client.ClientAppState;
import explosions.client.message.ClientMessage.ChatMessage;
import explosions.client.message.ClientMessage.PlayerInputMessage;
import explosions.client.message.ClientMessage.PlayerLookMessage;
import explosions.server.ServerAppState;
import explosions.server.message.ServerMessage.AnimationMessage;
import explosions.server.message.ServerMessage.AudioMessage;
import explosions.server.message.ServerMessage.RemoveMessage;
import explosions.server.message.ServerMessage.SpawnMessage;
import explosions.server.message.ServerMessage.UpdateLocationMessage;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This is the Main Class of your Game. You should only do initialization here.
 * Move your Logic into AppStates or Controls
 *
 * @author Soverliss
 */
public class ExplosionsSometimes extends SimpleApplication {
    
    private Nifty nifty;
    
    @Override
    public void simpleInitApp() {
        Serializer.registerClass(ChatMessage.class);
        Serializer.registerClass(PlayerInputMessage.class);
        Serializer.registerClass(PlayerLookMessage.class);
        Serializer.registerClass(UpdateLocationMessage.class);
        Serializer.registerClass(SpawnMessage.class);
        Serializer.registerClass(NetworkMessage.class);
        Serializer.registerClass(AudioMessage.class);
        Serializer.registerClass(AnimationMessage.class);
        Serializer.registerClass(RemoveMessage.class);
        
        Logger.getLogger("de.lessvoid.nifty").setLevel(Level.SEVERE);
        Logger.getLogger("NiftyInputEventHandlingLog").setLevel(Level.SEVERE);
        NiftyJmeDisplay niftyDisplay = NiftyJmeDisplay.newNiftyJmeDisplay(
                assetManager, inputManager, audioRenderer, guiViewPort);
        nifty = niftyDisplay.getNifty();
        nifty.fromXml("Interface/screens.xml", "start", new MenuScreen(this));
        guiViewPort.addProcessor(niftyDisplay);
        flyCam.setDragToRotate(true);
    }
    
    public void startServer(String port) {
        final ServerAppState serverAppState = new ServerAppState(port);
        stateManager.attach(serverAppState);
        enqueue(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                serverAppState.startGame();
                return null;
            }
        });
    }
    
    public void startClient(String ipAddress) {
        stateManager.attach(new ClientAppState());
        nifty.gotoScreen("hud");
        flyCam.setDragToRotate(false);
    }
    
    public void quit() {
        this.stop();
    }
    
    @Override
    public void simpleUpdate(float tpf) {
    }
    
    @Override
    public void simpleRender(RenderManager rm) {
    }
    
}
