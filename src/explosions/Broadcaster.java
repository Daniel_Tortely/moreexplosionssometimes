package explosions;

/**
 *
 * @author Student
 */
public interface Broadcaster {

    void sendUpdate(NetworkMessage networkMessage);

}
