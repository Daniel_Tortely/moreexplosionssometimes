package explosions;

import com.jme3.math.Vector3f;
import com.jme3.scene.Node;

/**
 *
 * @author Roland Fuller
 */
public abstract class Factory {

    public static final String ID = "ID";
    public static final String AMMO = "Ammo";
    public static final String FLOOR = "Floor";
    public static final String PLAYER = "Player";
    public static final String ROCKET = "Rocket";
    public static final String AMMO_SPAWNER = "AmmoSpawner";
    protected Node rootNode;
    protected Broadcaster broadcaster;

    public Factory(Node rootNode, Broadcaster broadcaster) {
        this.rootNode = rootNode;
        this.broadcaster = broadcaster;
    }

    protected Node makeCore(Vector3f location, String name, int id) {
        Node node = new Node(name);
        node.setLocalTranslation(location);
        rootNode.attachChild(node);
        node.setUserData(ID, id);
        return node;
    }
}
