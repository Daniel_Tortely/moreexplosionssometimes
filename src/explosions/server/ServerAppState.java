package explosions.server;

import explosions.server.entity.EntityUtility;
import explosions.server.entity.PlayerControl;
import explosions.server.entity.ItemControl;
import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.collision.PhysicsCollisionListener;
import com.jme3.bullet.collision.PhysicsCollisionObject;
import com.jme3.bullet.collision.PhysicsRayTestResult;
import com.jme3.math.Vector3f;
import com.jme3.network.ConnectionListener;
import com.jme3.network.Filters;
import com.jme3.network.HostedConnection;
import com.jme3.network.Message;
import com.jme3.network.MessageListener;
import com.jme3.network.Network;
import com.jme3.network.Server;
import com.jme3.scene.Node;
import com.jme3.scene.SceneGraphVisitor;
import com.jme3.scene.Spatial;
import static explosions.Factory.ID;
import explosions.GameObjectFinder;
import explosions.NetworkMessage;
import explosions.client.message.ClientMessage.ChatMessage;
import explosions.client.message.ClientMessage.PlayerInputMessage;
import explosions.client.message.ClientMessage.PlayerLookMessage;
import explosions.server.entity.SpawnStrategy.AmmoStrategy;
import explosions.server.message.ServerMessage.AudioMessage;
import explosions.server.message.ServerMessage.RemoveMessage;
import explosions.server.message.ServerMessage.SpawnMessage;
import explosions.server.message.ServerMessage.UpdateLocationMessage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import explosions.Broadcaster;

/**
 *
 * @author Roland Fuller
 */
public class ServerAppState extends AbstractAppState implements Broadcaster, PhysicsCollisionListener, EntityUtility, WorldUtility, ConnectionListener, MessageListener<HostedConnection> {

    private BulletAppState bulletAppState;
    private ServerFactory serverFactory;
    private Node serverRootNode;
    private Server myServer;
    private Application app;
    private final String port;

    public ServerAppState(String port) {
        this.port = port;
    }

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);

        this.app = app;
        serverRootNode = new Node();

        ((SimpleApplication) app).getRootNode().attachChild(serverRootNode);

        bulletAppState = new BulletAppState();
        stateManager.attach(bulletAppState);
        bulletAppState.getPhysicsSpace().addCollisionListener(this);
        // bulletAppState.setDebugEnabled(true);
        serverFactory = new ServerFactory(this, this, bulletAppState, this, serverRootNode);

        try {
            myServer = Network.createServer(Integer.parseInt(port));
            myServer.addMessageListener(this, NetworkMessage.class);
            myServer.addMessageListener(this, SpawnMessage.class);
            myServer.addMessageListener(this, ChatMessage.class);
            myServer.addMessageListener(this, PlayerInputMessage.class);
            myServer.addMessageListener(this, PlayerLookMessage.class);
            myServer.addMessageListener(this, UpdateLocationMessage.class);
            myServer.addConnectionListener(this);
            myServer.start();
        } catch (IOException ex) {

        }

    }

    @Override
    public void cleanup() {
        super.cleanup();
        myServer.close();
    }

    /**
     * This is the method implemented by the PhysicsCollisionListener interface
     * which is automatically called by the physics engine any time two physics
     * objects touch
     *
     * @param event
     */
    @Override
    public void collision(PhysicsCollisionEvent event) {
        // We use our resolve method to see if either node A or node B involved in the collision
        // is a goal, paddle, ball....
        Spatial player = resolve("Player", event.getNodeA(), event.getNodeB());
        Spatial bullet = resolve("Bullet", event.getNodeA(), event.getNodeB());
        Spatial ammo = resolve("Ammo", event.getNodeA(), event.getNodeB());

        // Once resolved, we can see which objects we are dealing with.
        // Everything is in a nested if, because we only care about collisions with the ball and (other thing)
        if (bullet != null) {
            if (player != null) { // player one's goal touched the ball
                player.getControl(PlayerControl.class).hurt(25);
            }
            removeGameObject(bullet);
        }

        if (ammo != null) {
            if (player != null) { // player one's goal touched the ball
                player.getControl(PlayerControl.class).giveAmmo(25);
                ammo.getControl(ItemControl.class).take();
            }
            removeGameObject(ammo);
        }
    }

    private void removeGameObject(Spatial object) {
        sendUpdate(new RemoveMessage((int) object.getUserData(ID)));
        bulletAppState.getPhysicsSpace().removeAll(object);
        object.removeFromParent();
    }

    /**
     * A simple utility method which checks to see which of two nodes has the
     * name we're looking for
     *
     * @param name The name we're testing for
     * @param nodeA the first node involved in the collision
     * @param nodeB the second node involved in the collision
     * @return the node that had the name we were looking for
     */
    private Spatial resolve(String name, Spatial nodeA, Spatial nodeB) {
        if (nodeA.getName().equals(name)) {
            return nodeA;
        } else if (nodeB.getName().equals(name)) {
            return nodeB;
        }
        return null;
    }

    @Override
    public void die(Spatial spatial) {
        removeGameObject(spatial);
    }

    @Override
    public void connectionAdded(Server server, final HostedConnection conn) {

        app.enqueue(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                final Vector3f humanLocation = new Vector3f(20, 20, 0);
                int id = serverFactory.makeHuman(humanLocation);
                conn.send(new SpawnMessage(id, GameObject.PLAYER, humanLocation, true));

                myServer.broadcast(Filters.notIn(conn), new SpawnMessage(id, GameObject.PLAYER, humanLocation));

                final HostedConnection guy = conn;

                serverRootNode.depthFirstTraversal(new SceneGraphVisitor() {
                    @Override
                    public void visit(Spatial spatial) {
                        boolean sendable = true;
                        if (spatial.getName() != null) {
                            GameObject objectType = GameObject.FLOOR;
                            switch (spatial.getName()) {
                                case "Floor":
                                    objectType = GameObject.FLOOR;
                                    break;
                                case "Bullet":
                                    objectType = GameObject.MISSILE;
                                    break;
                                case "Player":
                                    objectType = GameObject.PLAYER;
                                    break;
                                case "Ammo":
                                    objectType = GameObject.AMMO;
                                    break;
                                default:
                                    sendable = false;
                                    break;
                            }
                            if (sendable) {
                                guy.send(new SpawnMessage((int) spatial.getUserData(ID), objectType, spatial.getLocalTranslation()));
                            }
                        }
                    }
                });
                myServer.broadcast(new AudioMessage(id, "Sound/Effects/Bang.wav"));
                return null;
            }
        });

    }

    @Override
    public void connectionRemoved(Server server, HostedConnection conn) {
    }

    public enum GameObject {
        FLOOR, PLAYER, MISSILE, AMMO
    };

    public void startGame() {

        serverFactory.makeFloor();

        final Vector3f botLocation = new Vector3f(0, 20, 0);
        serverFactory.makeBot(botLocation);

        serverFactory.makeSpawner(new Vector3f(0, 2, 10), new AmmoStrategy(serverFactory));
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);
    }

    public void handlePlayerInput(final int id, final String command, final boolean pressed) {
        List<Spatial> spatials = getGameObjectsById(id);
        if (spatials.get(0) != null) {
            PlayerControl pc = spatials.get(0).getControl(PlayerControl.class);
            pc.action(command, pressed);
        }
    }

    private void handlePlayerLook(int id, Vector3f lookDirection) {
        List<Spatial> spatials = getGameObjectsById(id);
        if (spatials.get(0) != null) {
            PlayerControl pc = spatials.get(0).getControl(PlayerControl.class);
            pc.look(lookDirection);
        }
    }

    private List<Spatial> getGameObjectsById(final int id) {
        GameObjectFinder gameObjectFinder = new GameObjectFinder(new GameObjectFinder.FindById(id));
        serverRootNode.depthFirstTraversal(gameObjectFinder);
        return gameObjectFinder.getFound();
    }

    private List<Spatial> getGameObjectsByControl(Object control) {
        GameObjectFinder gameObjectFinder = new GameObjectFinder(new GameObjectFinder.FindByController(control));
        serverRootNode.depthFirstTraversal(gameObjectFinder);
        return gameObjectFinder.getFound();
    }

    @Override
    public void sendUpdate(NetworkMessage networkMessage) {
        myServer.broadcast(networkMessage);
    }

    @Override
    public void messageReceived(HostedConnection source, final Message message) {
        app.enqueue(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                if (message instanceof ChatMessage) {
                    // do something with the message
                    ChatMessage helloMessage = (ChatMessage) message;
                    System.out.println(helloMessage.message);
                } else if (message instanceof PlayerInputMessage) {
                    PlayerInputMessage pim = (PlayerInputMessage) message;
                    handlePlayerInput(pim.id, pim.command, pim.pressed);
                } else if (message instanceof PlayerLookMessage) {
                    PlayerLookMessage plm = (PlayerLookMessage) message;
                    handlePlayerLook(plm.id, plm.lookDirection);
                }
                return null;
            }
        });

    }

    @Override
    public void rayCast(Vector3f from, Vector3f to, PhysicsCollisionObject sender, RayMessage rayMessage) {
        to = from.add(to.mult(rayMessage.getRange()));
        List<PhysicsRayTestResult> results = new ArrayList<>();
        bulletAppState.getPhysicsSpace().rayTest(from, to, results);
        for (int i = results.size() - 1; i >= 0; i--) {
            if (i < rayMessage.getPenetration()) {
                if (results.get(i).getCollisionObject().getUserObject() != null) {
                    rayHit((Spatial) results.get(i).getCollisionObject().getUserObject(), rayMessage);
                }
            }
        }
    }

    private void rayHit(Spatial spatial, RayMessage rayMessage) {
        PlayerControl pc = spatial.getControl(PlayerControl.class);
        if (pc != null) {
            pc.hit(rayMessage);
        }
    }

}
