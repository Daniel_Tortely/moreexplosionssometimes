package explosions.server.entity;

import static explosions.client.entity.PlayerControl.JUMP;
import java.util.Random;

/**
 *
 * @author Roland Fuller
 */
public class BotControl extends PlayerControl {

    public BotControl(EntityUtility entityUtility) {
        super(entityUtility);
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);

        if (new Random().nextFloat() > 0.99f) {
            action(JUMP, true);
            // action(SHOOT, true);
        } else {
            action(JUMP, false);
            //action(SHOOT, false);
        }
    }

}
