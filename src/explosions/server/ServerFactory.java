package explosions.server;

import explosions.server.entity.UpdateControl;
import explosions.server.entity.SpawnControl;
import explosions.server.entity.BotControl;
import explosions.server.entity.EntityUtility;
import explosions.server.entity.SpawnStrategy;
import explosions.server.entity.PlayerControl;
import explosions.server.entity.ItemControl;
import explosions.server.entity.HumanControl;
import explosions.server.weapons.RocketShooter;
import explosions.server.weapons.PistolShooter;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.PhysicsCollisionObject;
import com.jme3.bullet.collision.shapes.BoxCollisionShape;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.collision.shapes.SphereCollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import explosions.Factory;
import explosions.server.ServerAppState.GameObject;
import explosions.server.message.ServerMessage.SpawnMessage;
import explosions.server.weapons.Pistol;
import explosions.server.weapons.RocketLauncher;
import explosions.server.weapons.Shooter;
import explosions.Broadcaster;
import static explosions.server.RocketControl.ROCKET_COLLISION_RADIUS;
import static explosions.server.RocketControl.ROCKET_MASS;
import static explosions.server.RocketControl.ROCKET_VELOCITY;

/**
 *
 * @author Roland Fuller
 */
public class ServerFactory extends Factory {

    private BulletAppState bulletAppState;
    private EntityUtility entityUtility;
    private WorldUtility worldUtility;
    private int entityId = 0;

    public ServerFactory(WorldUtility worldUtility, EntityUtility entityUtility, BulletAppState bulletAppState, Broadcaster broadcaster, Node rootNode) {
        super(rootNode, broadcaster);
        this.bulletAppState = bulletAppState;
        this.entityUtility = entityUtility;
        this.worldUtility = worldUtility;
    }

    private RigidBodyControl givePhysics(float mass, Spatial spatial, CollisionShape collisionShape) {
        RigidBodyControl rigidBodyControl = new RigidBodyControl(collisionShape, mass);
        spatial.addControl(rigidBodyControl);
        bulletAppState.getPhysicsSpace().add(rigidBodyControl);
        return rigidBodyControl;
    }

    public int makeFloor() {
        Node container = makeCore(new Vector3f(0, 0, 0), FLOOR, ++entityId);
        givePhysics(0, container, new BoxCollisionShape(new Vector3f(100, 1, 100)));
        return entityId;
    }

    public int makeRocket(Vector3f location, Vector3f direction, float spawnPoint) {
        Vector3f offset = direction.mult(5).add(0, spawnPoint, 0);
        Node container = makeCore(location.add(offset), ROCKET, ++entityId);
        RigidBodyControl rigidBodyControl = givePhysics(ROCKET_MASS, container, new SphereCollisionShape(ROCKET_COLLISION_RADIUS));
        rigidBodyControl.setLinearVelocity(direction.mult(ROCKET_VELOCITY));
        rigidBodyControl.setGravity(Vector3f.ZERO);
        container.addControl(new RocketControl());
        container.addControl(new UpdateControl(broadcaster));
        broadcaster.sendUpdate(new SpawnMessage(entityId, GameObject.MISSILE, location));
        return entityId;
    }

    public int makeHuman(Vector3f location) {
        final HumanControl humanControl = new HumanControl(entityUtility);
        giveWeapons(humanControl);
        return makePlayer(location, humanControl);
    }

    public int makeBot(Vector3f location) {
        final BotControl botControl = new BotControl(entityUtility);
        giveWeapons(botControl);
        return makePlayer(location, botControl);
    }

    private int makePlayer(Vector3f location, PlayerControl control) {
        Node container = makeCore(location, PLAYER, ++entityId);
        final UpdateControl updateControl = new UpdateControl(broadcaster);
        container.addControl(updateControl);
        control.setUpdateControl(updateControl);
        container.addControl(control);
        bulletAppState.getPhysicsSpace().add(container);
        return entityId;
    }

    private void giveWeapons(PlayerControl control) {
        Shooter rocketShooter = new RocketShooter(this, control);
        Shooter bulletShooter = new PistolShooter(this, control);
        final RocketLauncher rocketLauncher = new RocketLauncher(rocketShooter, control.getCache());
        control.giveWeapon(rocketLauncher);
        final Pistol pistol = new Pistol(bulletShooter, control.getCache());
        control.giveWeapon(pistol);
    }

    public void makeBullet(Vector3f location, Vector3f viewDirection, PhysicsCollisionObject physicsCollisionObject, float spawnPoint) {
        location.addLocal(new Vector3f(0, spawnPoint, 0));
        worldUtility.rayCast(location, viewDirection, physicsCollisionObject, new RayMessage(100, 25, 500));
    }

    public void makeAmmo(Vector3f location, SpawnControl spawnControl) {
        Node container = makeCore(location, AMMO, ++entityId);
        givePhysics(0, container, new BoxCollisionShape(new Vector3f(1, 1, 1)));
        container.addControl(new UpdateControl(broadcaster));
        container.addControl(new ItemControl(spawnControl));
        broadcaster.sendUpdate(new SpawnMessage(entityId, GameObject.AMMO, location));
    }

    public void makeSpawner(Vector3f location, SpawnStrategy strategy) {
        Node container = makeCore(location, AMMO_SPAWNER, ++entityId);
        container.addControl(new SpawnControl(strategy));
    }

}
