package explosions;

import com.jme3.scene.SceneGraphVisitor;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.Control;
import static explosions.Factory.ID;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Roland Fuller
 */
public class GameObjectFinder<T> implements SceneGraphVisitor {

    private List<T> found;
    private FindCondition<T> findCondition;

    public GameObjectFinder(FindCondition findCondition) {
        this.findCondition = findCondition;
        found = new ArrayList<T>();
    }

    @Override
    public void visit(Spatial spatial) {
        getFound().add(findCondition.check(spatial));
    }

    public List<T> getFound() {
        while (found.remove(null));
        return found;
    }

    public static interface FindCondition<T> {

        public T check(Spatial spatial);
    }

    public static class FindById implements FindCondition<Spatial> {

        private final int id;

        public FindById(int id) {
            this.id = id;
        }

        @Override
        public Spatial check(Spatial spatial) {
            final Object spatialID = spatial.getUserData(ID);
            if (spatialID != null) {
                if (id == (int) spatialID) {
                    return spatial;
                }
            }
            return null;
        }
    }

    public static class FindByController implements FindCondition<Spatial> {

        private final Object control;

        public FindByController(Object abstractControl) {
            this.control = abstractControl;
        }

        @Override
        public Spatial check(Spatial spatial) {
            for (int i = 0; i < spatial.getNumControls(); i++) {
                if (spatial.getControl(i) == control) {
                    return spatial;
                }
            }
            return null;
        }
    }

    public static class FindByControllerType implements FindCondition<Control> {

        private final Class control;

        public FindByControllerType(Class abstractControl) {
            this.control = abstractControl;
        }

        @Override
        public Control check(Spatial spatial) {
            for (int i = 0; i < spatial.getNumControls(); i++) {
                if (spatial.getControl(i).getClass().equals(control)) {
                    return spatial.getControl(i);
                }
            }
            return null;
        }
    }
}
